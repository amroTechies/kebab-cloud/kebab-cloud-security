--when I started thinking about a DB server and insert statements to create tables all I had to do in spring boot app
--regarding database was to first add H2 dependency in pom.xml then add file named schema.sql in the path resources
--then start spring boot app then you will find in the logs below statement:
--: H2 console available at '/h2-console'. Database available at 'jdbc:h2:mem:testdb'
-- so just hit the url http://localhost:8080/h2-console and then copy past this text 'jdbc:h2:mem:testdb' inside the field JDBC URL: then hit connect
--ad u r done .....no need to do the hints thats appears in this sql file in yellow (if they appear for u) like : "configure data source" or "change dialect to" at least at this stage :)
--this file is not needed for project kebab-cloud-no-DB because we dont use DB fetch or insert in this project
create table if not exists Ingredient (
id varchar(4) not null,
name varchar(25) not null,
type varchar(10) not null
);
create table if not exists Kebab (
id identity,
name varchar(50) not null,
createdAt timestamp not null
);
create table if not exists Kebab_Ingredients (
kebab_id bigint not null,
ingredients_id varchar(4) not null
);
alter table Kebab_Ingredients
add foreign key (kebab_id) references Kebab(id);
alter table Kebab_Ingredients
add foreign key (ingredients_id) references Ingredient(id);
create table if not exists Kebab_Order (
id identity,
deliveryName varchar(50) not null,
deliveryStreet varchar(50) not null,
deliveryCity varchar(50) not null,
deliveryState varchar(2) not null,
deliveryZip varchar(10) not null,
ccNumber varchar(16) not null,
ccExpiration varchar(5) not null,
ccCVV varchar(3) not null,
placedAt timestamp not null
);
create table if not exists Kebab_Order_Kebabs (
order_id bigint not null,
kebabs_id bigint not null
);
alter table Kebab_Order_Kebabs
add foreign key (order_id) references Kebab_Order(id);
alter table Kebab_Order_Kebabs
add foreign key (kebabs_id) references Kebab(id);

delete from Kebab_Order_Kebabs;
delete from Kebab_Ingredients;
delete from Kebab;
delete from Kebab_Order;

delete from Ingredient;
insert into Ingredient (id, name, type)
values ('FLTO', 'Flour', 'WRAP');
insert into Ingredient (id, name, type)
values ('COTO', 'Corn', 'WRAP');
insert into Ingredient (id, name, type)
values ('GRBF', 'Ground Beef', 'PROTEIN');
insert into Ingredient (id, name, type)
values ('CARN', 'Carnitas', 'PROTEIN');
insert into Ingredient (id, name, type)
values ('TMTO', 'Diced Tomatoes', 'VEGGIES');
insert into Ingredient (id, name, type)
values ('LETC', 'Lettuce', 'VEGGIES');
insert into Ingredient (id, name, type)
values ('CHED', 'Cheddar', 'CHEESE');
insert into Ingredient (id, name, type)
values ('JACK', 'Monterrey Jack', 'CHEESE');
insert into Ingredient (id, name, type)
values ('SLSA', 'Salsa', 'SAUCE');
insert into Ingredient (id, name, type)
values ('SRCR', 'Sour Cream', 'SAUCE');



create table if not exists user (
id identity,
username varchar(50) not null,
password varchar(150) not null,
fullname varchar(50) not null,
street varchar(50) not null,
city varchar(50) not null,
state varchar(50) not null,
zip varchar(50) not null,
phoneNumber varchar(50) not null,
enabled varchar(50) ,
);
--insert into users (username, password, enabled)
--values ('amro', '{noop}123', 'true');

create table if not exists authorities (
username varchar(50) not null,
authority varchar(50) not null
);
insert into authorities (username, authority)
values ('amro', 'Admin');