package com.amromalkawi.technology.kebabcloud;


import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Data
@RequiredArgsConstructor
@NoArgsConstructor(access= AccessLevel.PRIVATE, force=true)
@Entity
public class Ingredient {

    @Id
    private final String id;
    private final String name;

    @Enumerated(EnumType.STRING)
    private final Type type;

    /**if we dont use this annotation @Enumerated then u will get below exception when u hit the link that take u to design page:
     Caused by: org.h2.jdbc.JdbcSQLDataException: Data conversion error converting "WRAP" [22018-199]
     Caused by: java.lang.NumberFormatException: For input string: "WRAP"
     at java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)
     at java.lang.Integer.parseInt(Integer.java:580)
     ===== it is better if we use the integer value of Enum because it need less space .....check:
     https://vladmihalcea.com/the-best-way-to-map-an-enum-type-with-jpa-and-hibernate/

     @Enumerated
     @Column(columnDefinition = "smallint")
     private final Type type;
     **/


    public static enum Type {
        WRAP, PROTEIN, VEGGIES, CHEESE, SAUCE
    }
}
/**
 * In addition to the JPA-specific annotations, you’ll also note that you’ve added a
 * @NoArgsConstructor annotation at the class level. JPA requires that entities have a no-
 * arguments constructor, so Lombok’s @NoArgsConstructor does that for you. You
 * don’t want to be able to use it, though, so you make it private by setting the access
 * attribute to AccessLevel.PRIVATE . And because there are final properties that must
 * be set, you also set the force attribute to true , which results in the Lombok-generated
 * constructor setting them to null .
 * You also add a @RequiredArgsConstructor . The @Data implicitly adds a required
 * arguments constructor, but when a @NoArgsConstructor is used, that constructor gets
 * removed. An explicit @RequiredArgsConstructor ensures that you’ll still have a
 * required arguments constructor in addition to the private no-arguments constructor.
 **/