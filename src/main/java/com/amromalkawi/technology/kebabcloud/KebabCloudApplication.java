package com.amromalkawi.technology.kebabcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KebabCloudApplication {

    public static void main(String[] args) {
        SpringApplication.run(KebabCloudApplication.class, args);
    }

//https://github.com/habuma/spring-in-action-5-samples
}
