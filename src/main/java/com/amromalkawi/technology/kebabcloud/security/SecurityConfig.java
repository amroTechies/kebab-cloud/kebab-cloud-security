package com.amromalkawi.technology.kebabcloud.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    DataSource dataSource;  //needed for useInDBAuthentication


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        //we implemented basic authentication/authorization only using this class ...we did not even add login html page...when u add this class it will call html spring ready code
        // because we wanted to add more functionality like registration form we added User class and UserRepositroy and UserDetailsService and RegistrationController and register.html and login.html
        //imagine only with this class we were able to add security to our app ...amazing



       // useInMemoryAuthentication(auth);
        //useInDBAuthentication(auth);
        //useLdapAuthentication(auth);
        useCustomisedWithPageInDBAuth(auth);

    }




    private void useInMemoryAuthentication(AuthenticationManagerBuilder auth)throws Exception   {
        auth
                .inMemoryAuthentication()
                .withUser("buzz")
                .password("{noop}infinity")
                .authorities("ROLE_USER")
                .and()
                .withUser("woody")
                .password("{noop}bullseye")
                .authorities("ROLE_USER");
    }


    private void useInDBAuthentication(AuthenticationManagerBuilder auth) throws Exception{
        auth
                .jdbcAuthentication()
                .dataSource(dataSource);

        /**in order to the above to work u need below scripts to be run : Note the psw='{noop}123' , noop here mean no encoding
         * u should know that the book is not updated to the latest spring security so refer to https://spring.io/blog/2017/11/01/spring-security-5-0-0-rc1-released#password-storage-format
         *
         *

         create table if not exists users (
         username varchar(50) not null,
         password varchar(50) not null,
         enabled varchar(50) not null,
         );
         insert into users (username, password, enabled)
         values ('amro', '{noop}123', 'true');

         create table if not exists authorities (
         username varchar(50) not null,
         authority varchar(50) not null
         );
         insert into authorities (username, authority)
         values ('amro', 'Admin');
         *
         */
    }


    private void useLdapAuthentication(AuthenticationManagerBuilder auth)
        throws Exception {
            auth
                    .ldapAuthentication()
                    .userSearchFilter("(uid={0})")
                    .groupSearchFilter("member={0}");
    }
    /**
     * There are currently two authentication strategies supplied with Spring Security:
     *
     *     1-Authentication directly to the LDAP server ("bind" authentication).: **this is like provide
     *     the userName and psw and u get a response if its successful
     *
     *     2-Password comparison, where the password supplied by the user is compared with the one stored in the
     *     repository. This can either be done by retrieving the value of the password attribute and checking it
     *     locally or by performing an LDAP "compare" operation, where the supplied password is passed to the server
     *     for comparison and the real password value is never retrieved.
     *     ** here u also provide userName and psw but u ask the ldap server to compare the given psw with any field
     *     or attribute u want ..I guess this would mean like u can have mutliple passwords for a user .
     *     **Some LDAP servers will be configured so that certain individual users are not allowed to bind
     *     directly to the server, or so that anonymous binding (what we have been using for user search up
     *     until this point) is disabled. This tends to occur in very large organizations which want a restricted
     *     set of users to be able to read information from the directory.     *
     *      In these cases, the standard Spring Security LDAP authentication strategy will not work,
     *      and an alternative strategy must be used,
     *      implemented by o.s.s.ldap.authentication.PasswordComparisonAuthenticator (a sibling class of
     *      BindAuthenticator):
     */


    /**
     * By default, Spring Security’s LDAP authentication assumes that the LDAP server is
     * listening on port 33389 on localhost. But if your LDAP server is on another machine,
     * you can use the contextSource() method to configure the location:
     * @Override
     * protected void configure(AuthenticationManagerBuilder auth)
     * throws Exception {
     * auth
     * .ldapAuthentication()
     * .userSearchBase("ou=people")
     * .userSearchFilter("(uid={0})")
     * .groupSearchBase("ou=groups")
     * .groupSearchFilter("member={0}")
     * .passwordCompare()
     * .passwordEncoder(new BCryptPasswordEncoder())
     * .passwordAttribute("passcode")
     * .contextSource()
     * .url("ldap://tacocloud.com:389/dc=tacocloud,dc=com");
     * }
     */


    /**
     * If you don’t happen to have an LDAP server lying around waiting to be authenticated
     * against, Spring Security can provide an embedded LDAP server for you. Instead of set-
     * ting the URL to a remote LDAP server, you can specify the root suffix for the embed-
     * ded server via the root() method:
     * @Override
     * protected void configure(AuthenticationManagerBuilder auth)
     * throws Exception {
     * auth
     * .ldapAuthentication()
     * .userSearchBase("ou=people")
     * .userSearchFilter("(uid={0})")
     * .groupSearchBase("ou=groups")
     * .groupSearchFilter("member={0}")
     * .passwordCompare()
     * .passwordEncoder(new BCryptPasswordEncoder())
     * .passwordAttribute("passcode")
     * .contextSource()
     * .root("dc=tacocloud,dc=com");
     * }
     */



/** @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ **/
/** This part is used when u want to customize ur login and add form regestration as Spring provide only ready to use code for login not for registration**/

@Qualifier("userRepositoryUserDetailsService")   //it seems in spring there is another bean that implements UserDetailsService named  inMemoryUserDetailsManager so thats why we added the qualified annotation
@Autowired
private UserDetailsService userDetailsService;

 private void useCustomisedWithPageInDBAuth(AuthenticationManagerBuilder auth) throws Exception {
 auth
 .userDetailsService(userDetailsService)
         .passwordEncoder(encoder());
 }

    @Bean
    public PasswordEncoder encoder() {
     //return PasswordEncoderFactories.createDelegatingPasswordEncoder();
        return new StandardPasswordEncoder("53cr3t");// note here it does not matter what the encoding is
                    // because user will enter his psw in register form then spring will encode the value and save it in DB
                    // I mean here u dont need insert sql statements to enter encoded passwords
    }


    /**The security requirements for Kebab Cloud should require that a user be authenticated
    before designing kebabs or placing orders. But the homepage, login page, and registra-
    tion page should be available to unauthenticated users.
    To configure these security rules, let me introduce you to WebSecurityConfigurer-
    Adapter ’s other configure() method:**/
    /** Among the many things you can con-
     figure with HttpSecurity are these:
      Requiring that certain security conditions be met before allowing a request to
     be served
      Configuring a custom login page
      Enabling users to log out of the application
      Configuring cross-site request forgery protection **/
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/design", "/orders")
                .access("hasRole('ROLE_USER')")
                .antMatchers("/", "/**").access("permitAll")
                //end::authorizeRequests[]

                .and()
                .formLogin().loginPage("/login")
                //end::customLoginPage[]

                // tag::enableLogout[]
                .and()
                .logout()
                .logoutSuccessUrl("/")
                // end::enableLogout[]

                // Make H2-Console non-secured; for debug purposes
                // tag::csrfIgnore[]
                .and()
                .csrf()
                .ignoringAntMatchers("/h2-console/**")
                // end::csrfIgnore[]

                // Allow pages to be loaded in frames from the same origin; needed for H2-Console
                // tag::frameOptionsSameOrigin[]
                .and()
                .headers()
                .frameOptions()
                .sameOrigin()
        // end::frameOptionsSameOrigin[]

        //tag::authorizeRequests[]
        //tag::customLoginPage[]
        ;
    }

    /** Just as important as logging into an application is logging out. To enable logout, you
     simply need to call logout on the HttpSecurity object:
     .and()
     .logout()
     .logoutSuccessUrl("/")
     This sets up a security filter that intercepts POST requests to /logout. Therefore, to
     provide logout capability, you just need to add a logout form and button to the views
     in your application:
     <form method="POST" th:action="@{/logout}">
     <input type="submit" value="Logout"/>
     </form>      we already added it in login.html page and it worked ..just anyhere**/



   // I did not implement part (4.4 Knowing your user) of the book
/** @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ **/
}