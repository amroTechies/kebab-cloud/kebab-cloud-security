package com.amromalkawi.technology.kebabcloud;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
@Entity
public class Kebab {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)   // I used @GeneratedValue(strategy = GenerationType.AUTO) the same as in the book but i got this exception:Caused by: org.h2.jdbc.JdbcSQLSyntaxErrorException: Sequence "HIBERNATE_SEQUENCE" not found; SQL statement:
    private Long id;


    private Date createdAt;


    @NotNull
    @Size(min=5, message="Name must be at least 5 characters long")
    private String name;

    @NotNull(message = "You must choose at least 2 ingredient")
    @Size(min=2, message="You must choose at least 2 ingredient")
    @ManyToMany(targetEntity=Ingredient.class)
    //private List<String> ingredients;   **** nfsk tfham laish hon zab6at ..gareeb m3 il jpa
    private List<Ingredient> ingredients= new ArrayList<>();      /**we had to change coloumn names of table
                                                                     Kebab_Ingredients from kebab to kebab_id
                                                                    and from ingredient to ingredients_id
                                                                    so that it work with he jpa automatic mapping:)**/




    @PrePersist
    void createdAt() {
        this.createdAt = new Date();
    }




}
