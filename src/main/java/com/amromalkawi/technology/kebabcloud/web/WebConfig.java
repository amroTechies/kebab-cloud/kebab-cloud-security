package com.amromalkawi.technology.kebabcloud.web;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("home");
        registry.addViewController("/abc").setViewName("home");
        registry.addViewController("/login");  // i dont know why we did not add .setViewName here ..I guess we can add .setViewName("login")
                                                      // since we added in SecurityConfig.java: (formLogin().loginPage("/login")) then this means we need to add need to provide a controller that handles requests at that path. Because
                                                         //our login page will be fairly simple—nothing but a view—it’s easy enough to declare
                                                            //it as a view controller in WebConfig

    }


}