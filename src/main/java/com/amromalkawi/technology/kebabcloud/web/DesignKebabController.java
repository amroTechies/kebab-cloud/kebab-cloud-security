package com.amromalkawi.technology.kebabcloud.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


import com.amromalkawi.technology.kebabcloud.Ingredient;
import com.amromalkawi.technology.kebabcloud.Kebab;
import com.amromalkawi.technology.kebabcloud.Order;
import com.amromalkawi.technology.kebabcloud.data.IngredientRepository;
import com.amromalkawi.technology.kebabcloud.data.KebabRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import com.amromalkawi.technology.kebabcloud.Ingredient.Type;

import lombok.extern.slf4j.Slf4j;

import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping("/design")
@SessionAttributes("order")   //you need the order to be present across multiple requests so that you can create
                              // multiple tacos and add them   to the order. The class-level @SessionAttributes
                            //  annotation specifies any model objects like the order attribute that should be kept in session and available across
                            //multiple requests
public class DesignKebabController {


    @ModelAttribute(name = "order")
    public Order order() {
        return new Order();
    }




    private final IngredientRepository ingredientRepo;
    private KebabRepository designRepo;

    @Autowired
    public DesignKebabController(IngredientRepository ingredientRepo,KebabRepository designRepo) {
        this.designRepo = designRepo;
        this.ingredientRepo = ingredientRepo;
    }

    @ModelAttribute
    public void addIngredientsToModel(Model model) {
        List<Ingredient> ingredients = new ArrayList<>();
        ingredientRepo.findAll().forEach(i -> ingredients.add(i));

        Type[] types = Ingredient.Type.values();
        for (Type type : types) {
            model.addAttribute(type.toString().toLowerCase(),
                    filterByType(ingredients, type));
        }
    }


    @GetMapping
    public String showDesignForm(Model model) {
       model.addAttribute("kebabDesign", new Kebab());
        return "design";
    }




    private List<Ingredient> filterByType(
            List<Ingredient> ingredients, Type type) {
        return ingredients
                .stream()
                .filter(x -> x.getType().equals(type))
                .collect(Collectors.toList());
    }



    @PostMapping
    public String processDesign(@Valid @ModelAttribute("kebabDesign") Kebab kebabDesign,
                                Errors errors,@ModelAttribute Order order) {  //The Order parameter is annotated with @ModelAttribute to indicate that its
                                                                                //value should come from the model and that Spring MVC shouldn’t attempt to bind
                                                                                // request parameters to it.
        if (errors.hasErrors()) {
               return "design";
           }
         log.info("Processing kebab design: " + kebabDesign);
        Kebab saved = designRepo.save(kebabDesign);
        order.addDesign(saved);
        return "redirect:/orders/current";
    }
}



   /** @ModelAttribute annotation on order() ensures that an Order object will be created
     in the model.

    if we dont add below then we will git below exception
    @ModelAttribute(name = "order")
    public Order order() {
    return new Order();
    }
    Exception:
    There was an unexpected error (type=Internal Server Error, status=500).
    Expected session attribute 'order'
    org.springframework.web.HttpSessionRequiredException: Expected session attribute 'order'
    at org.springframework.web.method.annotation.ModelFactory.initModel(ModelFactory.java:112)
    at



    below is equal to model.addAttribute("kebabDesign", new Kebab()); in the method of showDesignForm
    @ModelAttribute(name = "kebabDesign")
    public Kebab kebab() {
    return new Kebab();
    }



    **/


//@RequestMapping General-purpose request handling
//@GetMapping Handles HTTP GET requests
//@PostMapping Handles HTTP POST requests
//@PutMapping Handles HTTP PUT requests
//@DeleteMapping Handles HTTP DELETE requests
//@PatchMapping Handles HTTP PATCH requests