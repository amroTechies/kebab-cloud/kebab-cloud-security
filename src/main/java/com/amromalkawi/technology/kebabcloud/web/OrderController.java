package com.amromalkawi.technology.kebabcloud.web;


import javax.validation.Valid;

import com.amromalkawi.technology.kebabcloud.data.OrderRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import lombok.extern.slf4j.Slf4j;
import com.amromalkawi.technology.kebabcloud.Order;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

@Slf4j
@Controller
@RequestMapping("/orders")
@SessionAttributes("order")
public class OrderController {

    private OrderRepository orderRepo;
    public OrderController(OrderRepository orderRepo) {
        this.orderRepo = orderRepo;
    }

    @GetMapping("/current")
    public String orderForm(Model model) {
        //model.addAttribute("order", new Order());    .now we have issue when we tried to save order in session
                                                        // ..this line will overwrite the session attribute order causing it to lost its state
                                                        //this line was here to solve something in Kebab-cloud-no-DB project
        return "orderForm";
    }



    @PostMapping
    public String processOrder(@Valid Order order, Errors errors,
                  SessionStatus sessionStatus) {
        if (errors.hasErrors()) {
            return "orderForm";
        }
        log.info("Order submitted: " + order);
        orderRepo.save(order);
        sessionStatus.setComplete(); /**Once the order is saved, you don’t need it hanging around in a session anymore.
         In fact, if you don’t clean it out, the order remains in session, including its associated
         kebabs, and the next order will start with whatever kebabs the old order contained.
         Therefore, the processOrder() method asks for a SessionStatus parameter and
         calls its setComplete() method to reset the session. **/
        return "redirect:/";    //redirect to home page
    }
}