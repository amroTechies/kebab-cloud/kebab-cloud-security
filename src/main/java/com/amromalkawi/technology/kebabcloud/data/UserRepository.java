package com.amromalkawi.technology.kebabcloud.data;

import com.amromalkawi.technology.kebabcloud.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
        User findByUsername(String username);

}
