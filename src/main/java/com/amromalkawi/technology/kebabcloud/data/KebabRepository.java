package com.amromalkawi.technology.kebabcloud.data;

import com.amromalkawi.technology.kebabcloud.Kebab;
import org.springframework.data.repository.CrudRepository;

public interface KebabRepository extends CrudRepository<Kebab, Long> {

}
/**
 * CrudRepository declares about a dozen methods for CRUD (create, read, update,
 * delete) operations. Notice that it’s parameterized, with the first parameter being the
 * entity type the repository is to persist, and the second parameter being the type of the
 * entity ID property.
 * **/