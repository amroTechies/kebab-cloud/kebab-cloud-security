package com.amromalkawi.technology.kebabcloud.data;

import com.amromalkawi.technology.kebabcloud.Ingredient;
import org.springframework.data.repository.CrudRepository;

public interface IngredientRepository extends CrudRepository<Ingredient, String> {


}

/**
 * You might be thinking that you need to
 * write the implementations for all three, including the dozen methods for each imple-
 * mentation. But that’s the good news about Spring Data JPA—there’s no need to write
 * an implementation! When the application starts, Spring Data JPA automatically gener-
 * ates an implementation on the fly. This means the repositories are ready to use from
 * the get-go. Just inject them into the controllers like you did for the JDBC-based imple-
 * mentations, and you’re done.
 * **/