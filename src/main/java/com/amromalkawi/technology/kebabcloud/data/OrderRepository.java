package com.amromalkawi.technology.kebabcloud.data;

import com.amromalkawi.technology.kebabcloud.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Long> {

    List<Order> findByDeliveryZip(String deliveryZip);
    List<Order> readOrdersByDeliveryZipAndPlacedAtBetween(
            String deliveryZip, Date startDate, Date endDate);
    //List<Order> findByDeliveryToAndDeliveryCityAllIgnoresCase(   w hay kaman ma ishtaglat ..not compiled
      //      String deliveryTo, String deliveryCity);
    /**
     * we done need the above two abstracts methods in our project
     * but I have added it
     * here to show you that if u needed any extra case then all u have to
     * do is write an abstract method and spring data will do the implementation
     * for u by following rules of naming of the method
     *
     * for more info check page 82 of the spring in action book
     * **/

    /** in case u want even more speicifc method u can
     add the query like below for ur own need
     but it did not compile..I dont know why !!!!**/
    //@Query("Order o where o.deliveryCity='Seattle'")
   // List<Order> readOrdersDeliveredInSeattle();
}
